# symfony/mailer

[**symfony/mailer**](https://packagist.org/packages/symfony/mailer)
[![PHPPackages Rank](https://pages-proxy.gitlab.io/phppackages.org/symfony/mailer/rank.svg)](http://phppackages.org/p/symfony/mailer)
[![PHPPackages Referenced By](https://pages-proxy.gitlab.io/phppackages.org/symfony/mailer/referenced-by.svg)](http://phppackages.org/p/symfony/mailer)
Sending emails. [symfony.com/mailer](https://symfony.com/mailer)

## Official documentation

## Unofficial documentation
* [*How to use symfony/mailer without the Symfony framework*
  ](https://dev.to/doekenorg/how-to-use-symfony-mailer-without-the-symfony-framework-123g)
  2021-09 Doeke Norg
  * https://doeken.org/blog/using-symfony-mailer-without-framework
* [*Symfony Notifier*](https://speakerdeck.com/fabpot/symfony-notifier)
  2019-09 Fabien Potencier
* (fr) [*Symfony Mailer — Et si je t’envoi un mail*
  ](https://medium.com/@gary.houbre/symfony-mailer-et-si-je-tenvoi-un-mail-fe07f518658)
  2019-08 Gary Houbre
* (fr) [*Preview du nouveau composant Mailer de Symfony*
  ](https://blog.netinfluence.ch/2019/04/05/preview-du-nouveau-composant-mailer-de-symfony/)
  2019-04 [Romaric](https://blog.netinfluence.ch/author/romaric/)
